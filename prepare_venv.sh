#!/bin/bash
set -e

python3 -m venv --system-site-packages --prompt NetDaemon --upgrade-deps venv
source venv/bin/activate
venv/bin/pip install -r requirements.txt
