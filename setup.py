#  CARPI DISPLAY DAEMON
#  (C) 2021, Raphael "rGunti" Guntersweiler
#  Licensed under MIT
from setuptools import setup


def read_version():
    with open('VERSION.txt', 'r') as f:
        return f.read().strip()


setup(name='carpi-netdaemon',
      version=read_version(),
      description='CarPi Network Daemon',
      author='Raphael "rGunti" Guntersweiler',
      author_email='raphael@rgunti.ch',
      license='MIT',
      packages=['netdaemon'],
      install_requires=[
            'carpi-commons @ git+https://gitlab.com/car-pi/CarPi-Commons.git@master',
            'carpi-daemoncommons @ git+https://gitlab.com/car-pi/CarPi-DaemonCommons.git@master',
            'carpi-redisdatabus @ git+https://gitlab.com/car-pi/CarPi-RedisDataBus.git@master',
            'pythonping >= 1.1.0',
            'netifaces'
      ],
      classifiers=[
            'Development Status :: 3 - Alpha',
            'License :: OSI Approved :: MIT License',
            'Programming Language :: Python :: 3.9'
      ],
      zip_safe=False,
      include_package_data=True)
