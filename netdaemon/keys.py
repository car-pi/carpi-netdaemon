#  CARPI DISPLAY DAEMON
#  (C) 2021, Raphael "rGunti" Guntersweiler
#  Licensed under MIT

from redisdatabus.bus import TypedBusListener

KEY_BASE = 'carpi.netd.'


def build_key(type: str, name: str) -> str:
    global KEY_BASE
    return "{}{}{}".format(type, KEY_BASE, name)


# --- SYSTEM -----------------------------------------------------------------------------------------------------------
# Daemon Ping
KEY_SYS_TIMESTAMP = build_key(TypedBusListener.TYPE_PREFIX_STRING, 'systimestamp')

# --- BASIC INFO -------------------------------------------------------------------------------------------------------
# Online
KEY_IS_ONLINE = build_key(TypedBusListener.TYPE_PREFIX_BOOL, 'is_online')
# When the next state update for KEY_IS_ONLINE can be expected
KEY_IS_ONLINE_EXPECT_NEXT = build_key(TypedBusListener.TYPE_PREFIX_STRING, 'is_online.expect_next')

# --- PING -------------------------------------------------------------------------------------------------------------
# Min Ping
KEY_PING_MIN = build_key(TypedBusListener.TYPE_PREFIX_FLOAT, 'ping.min')
# Avg Ping
KEY_PING_AVG = build_key(TypedBusListener.TYPE_PREFIX_FLOAT, 'ping.avg')
# Max Ping
KEY_PING_MAX = build_key(TypedBusListener.TYPE_PREFIX_FLOAT, 'ping.max')
# Ping IP
KEY_PING_IP = build_key(TypedBusListener.TYPE_PREFIX_STRING, 'ping.ip')
# Ping Count
KEY_PING_COUNT = build_key(TypedBusListener.TYPE_PREFIX_INT, 'ping.count')
# Packet Loss
KEY_PING_PACKET_LOSS = build_key(TypedBusListener.TYPE_PREFIX_FLOAT, 'packet_loss')
# Ping Error
KEY_PING_ERROR = build_key(TypedBusListener.TYPE_PREFIX_STRING, 'ping.last_error')
# Last Ping Timestamp
KEY_PING_TIMESTAMP = build_key(TypedBusListener.TYPE_PREFIX_STRING, 'ping.timestamp')
# Next expected Ping Timestamp
KEY_PING_NEXT_TIMESTAMP = build_key(TypedBusListener.TYPE_PREFIX_STRING, 'ping.next_timestamp')

KEYS_PING = [
    KEY_PING_MIN,
    KEY_PING_AVG,
    KEY_PING_MAX,
    KEY_PING_IP,
    KEY_PING_COUNT,
    KEY_PING_PACKET_LOSS,
    KEY_PING_ERROR,
    KEY_PING_TIMESTAMP,
    KEY_PING_NEXT_TIMESTAMP
]

# --- IP ADDRESSES -----------------------------------------------------------------------------------------------------
# Ethernet Adapter
KEY_IP_ETH = build_key(TypedBusListener.TYPE_PREFIX_STRING, 'ip.eth')
# USB Adapter
KEY_IP_USB = build_key(TypedBusListener.TYPE_PREFIX_STRING, 'ip.usb')
# WiFi Adapter
KEY_IP_WIFI = build_key(TypedBusListener.TYPE_PREFIX_STRING, 'ip.wifi')
# All IP addresses (comma-separated)
KEY_IP_ALL = build_key(TypedBusListener.TYPE_PREFIX_STRING, 'ip.all')

KEYS_IPS = [
    KEY_IP_ETH,
    KEY_IP_USB,
    KEY_IP_WIFI,
    KEY_IP_ALL
]
