#  CARPI DISPLAY DAEMON
#  (C) 2021, Raphael "rGunti" Guntersweiler
#  Licensed under MIT
import random
import time
from logging import Logger
from threading import Thread
from typing import Optional

from carpicommons.log import logger


class CancellationToken(object):
    def __init__(self):
        self._id = random.randint(0, 0xFFFF)
        self._invoked: Optional[float] = None

    def invoke(self):
        self._invoked = time.time()

    @property
    def was_invoked(self):
        return self._invoked is not None

    def __str__(self) -> str:
        return 'CancellationToken %s (invoke=%s)' % (hex(self._id), str(self._invoked))


def sleep_unless_invoked(token: CancellationToken,
                         duration: float,
                         sleep_steps: float = 1):
    if token.was_invoked:
        return

    time_target = time.time() + duration
    while not token.was_invoked and time.time() < time_target:
        time.sleep(sleep_steps)


class ThreadInfo(object):
    def __init__(self,
                 thread: Thread,
                 token: CancellationToken):
        self._thread = thread
        self._cancellation_token = token

    def start_thread(self):
        self._thread.start()

    def stop_thread(self,
                    wait_for_thread: bool = False,
                    thread_timeout: Optional[float] = None):
        self._cancellation_token.invoke()
        if wait_for_thread:
            self._thread.join(thread_timeout)


def dummy_thread(log: Logger, token: CancellationToken):
    log.info('Dummy Thread started')
    while not token.was_invoked:
        time.sleep(1)

    log.info('Dummy Thread ended')
