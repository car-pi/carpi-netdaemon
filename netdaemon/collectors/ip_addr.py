#  CARPI DISPLAY DAEMON
#  (C) 2021, Raphael "rGunti" Guntersweiler
#  Licensed under MIT
from threading import Thread
from typing import Callable, Dict, Any, Optional

import netifaces as ni

from carpicommons.log import logger

from netdaemon.collectors.base import ThreadInfo, CancellationToken, sleep_unless_invoked
from netdaemon.config import InterfaceConfig
from netdaemon.keys import KEY_IP_ETH, KEY_IP_USB, KEY_IP_WIFI


def ip_addr_thread(config: InterfaceConfig,
                   report_data_fn: Callable[[Dict[str, Any]], Any]) -> ThreadInfo:
    log = logger('IpAddrThread')
    token = CancellationToken()

    def thread():
        def _get_ip_addr(ifname: str) -> Optional[str]:
            try:
                return ni.ifaddresses(ifname)[ni.AF_INET][0]['addr']
            except KeyError:
                return 'N/V'
            except ValueError:
                return 'N/A'

        def _collect():
            log.info('Collecting interface IPs ...')
            eth_ip = _get_ip_addr(config.eth_interface)
            usb_ip = _get_ip_addr(config.usb_interface)
            wifi_ip = _get_ip_addr(config.wifi_interface)

            report_data_fn({
                KEY_IP_ETH: eth_ip,
                KEY_IP_USB: usb_ip,
                KEY_IP_WIFI: wifi_ip,
            })

        log.info('Started Interface Address Collection Thread ...')
        while not token.was_invoked:
            _collect()
            sleep_unless_invoked(token, config.interval)

        log.info('Interface Address Collection ended')

    return ThreadInfo(thread=Thread(target=thread),
                      token=token)
