#  CARPI DISPLAY DAEMON
#  (C) 2021, Raphael "rGunti" Guntersweiler
#  Licensed under MIT
import os
from datetime import datetime, timedelta
from threading import Thread
from typing import Callable, Dict, Any

import pythonping

from carpicommons.log import logger
from pythonping.executor import SuccessOn

from netdaemon.collectors.base import ThreadInfo, CancellationToken, sleep_unless_invoked, dummy_thread
from netdaemon.config import PingCheckConfig
from netdaemon.errors import NetDaemonError

import netdaemon.keys as net_keys


def ping_thread(config: PingCheckConfig,
                report_data_fn: Callable[[Dict[str, Any]], Any]) -> ThreadInfo:
    log = logger('PingThread')

    token = CancellationToken()

    def _thread():
        def _do_ping():
            log.info('Collecting ping information ...')
            results = []
            for ip in config.ips:
                if token.was_invoked:
                    log.info('Cancellation token invoked, stopping ping routine')

                report_data = {
                    net_keys.KEY_PING_IP: ip,
                }

                log.info('Pinging %s ...', ip)
                try:
                    ping = pythonping.ping(ip,
                                           count=config.count,
                                           interval=1,
                                           match=True)

                    log.info('Ping completed to %s took %.1fms avg with %.0f packets lost',
                             ip,
                             ping.rtt_avg_ms,
                             ping.packet_loss)

                    report_data[net_keys.KEY_PING_AVG] = ping.rtt_avg_ms
                    report_data[net_keys.KEY_PING_MIN] = ping.rtt_min_ms
                    report_data[net_keys.KEY_PING_MAX] = ping.rtt_max_ms
                    report_data[net_keys.KEY_PING_COUNT] = config.count
                    report_data[net_keys.KEY_PING_PACKET_LOSS] = ping.packet_loss

                    results.append(ping)
                except PermissionError as e:
                    log.error('Permission error while pinging %s: %s', ip, e.strerror)
                    report_data[net_keys.KEY_PING_ERROR] = '{}|ERROR_PERM|'.format(ip)
                    results.append(None)
                except RuntimeError as e:
                    log.error('Runtime error while pinging %s: %s', ip, e)
                    report_data[net_keys.KEY_PING_ERROR] = '{}|ERROR_RUNT|{}'.format(ip, e)
                    results.append(None)
                finally:
                    now = datetime.now()
                    next_ping = now + timedelta(seconds=config.interval)
                    report_data[net_keys.KEY_PING_TIMESTAMP] = now.isoformat()
                    report_data[net_keys.KEY_PING_NEXT_TIMESTAMP] = next_ping.isoformat()
                    report_data_fn(report_data)

            failed_pings = len(list(filter(lambda i: i is None, results)))
            dropped_pings = len(list(filter(lambda i: i is not None and not i.success(SuccessOn.Most), results)))
            # is_online = more than 2/3 of all pings were successful
            is_online = failed_pings + dropped_pings < len(config.ips) / 3 * 2
            log.info('Executed %.0f pings, %.0f failed, %.0f dropped; Verdict: is_online=%s',
                     len(config.ips),
                     failed_pings,
                     dropped_pings,
                     str(is_online))
            report_data_fn({
                net_keys.KEY_IS_ONLINE: int(is_online),
                net_keys.KEY_IS_ONLINE_EXPECT_NEXT: (datetime.now() + timedelta(seconds=config.interval)).isoformat()
            })

        log.info('Started Ping Checker Thread ...')

        while not token.was_invoked:
            _do_ping()
            log.info('Completed all pings, sleeping for %.1fs', config.interval)
            sleep_unless_invoked(token, config.interval)

        log.info('Ping Checker Thread ended')

    euid = os.geteuid()
    if euid != 0:
        log.error('Cannot run Ping Checker without root privileges! (EUID=%s)', euid)
        if config.enforce_root:
            raise NetDaemonError()
        return ThreadInfo(thread=Thread(target=dummy_thread, args=[log, token]),
                          token=token)

    return ThreadInfo(thread=Thread(target=_thread),
                      token=token)
