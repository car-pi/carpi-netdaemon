#  CARPI DISPLAY DAEMON
#  (C) 2021, Raphael "rGunti" Guntersweiler
#  Licensed under MIT
from carpicommons.errors import CarPiExitException


class NetDaemonError(CarPiExitException):
    EXIT_CODE = 0xD000

    def __init__(self):
        super().__init__(NetDaemonError.EXIT_CODE)
