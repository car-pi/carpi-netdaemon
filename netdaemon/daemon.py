#  CARPI DISPLAY DAEMON
#  (C) 2021, Raphael "rGunti" Guntersweiler
#  Licensed under MIT
from datetime import datetime
from logging import Logger
from time import sleep
from typing import Optional, List, Dict, Any

from carpicommons.log import logger
from daemoncommons.daemon import Daemon
from redisdatabus.bus import BusWriter

import netdaemon.keys as net_keys
from netdaemon.collectors.base import ThreadInfo
from netdaemon.collectors.ip_addr import ip_addr_thread
from netdaemon.config import get_ping_config, get_interface_config, PingCheckConfig, InterfaceConfig


class NetDaemon(Daemon):
    def __init__(self):
        super().__init__('NetDaemon')
        self._is_running = True
        self._log: Optional[Logger] = None
        self._bus: Optional[BusWriter] = None
        self._threads: List[ThreadInfo] = []

    def shutdown(self):
        if self._log:
            self._log.info('Shutting down daemon ...')
        self._is_running = False
        self._shutdown_threads()

    def _shutdown_threads(self):
        for t in self._threads:
            t.stop_thread(True, 10)

    def _register_and_start_thread(self, thread: ThreadInfo):
        thread.start_thread()
        self._threads.append(thread)

    def startup(self):
        self._log = log = logger(self.name)
        log.info('Starting up daemon ...')

        log.info('Reading configuration data ...')
        ping_config = get_ping_config(self._config)
        if_config = get_interface_config(self._config)

        # Constructing Bus Writer
        self._bus = bus = self._build_bus_writer()

        # Start all Collectors
        self._start_collectors(ping_config, if_config)

        log.info('Entering main loop')
        while self._is_running:
            # Always publish daemon ping
            bus.publish(net_keys.KEY_SYS_TIMESTAMP, datetime.now().isoformat())
            # Only daemon ping every second
            sleep(1)

    def _start_collectors(self,
                          ping_config: PingCheckConfig,
                          if_config: InterfaceConfig):
        from netdaemon.collectors.ping import ping_thread

        self._log.info('Constructing collector threads ...')
        self._register_and_start_thread(ping_thread(ping_config, lambda d: self._report_data(d)))
        self._register_and_start_thread(ip_addr_thread(if_config, lambda d: self._report_data(d)))

    def _build_bus_writer(self) -> BusWriter:
        self._log.info("Connecting to Redis instance ...")
        return BusWriter(host=self._get_config('Redis', 'Host', '127.0.0.1'),
                         port=self._get_config_int('Redis', 'Port', 6379),
                         db=self._get_config_int('Redis', 'DB', 0),
                         password=self._get_config('Redis', 'Password', None))

    def _report_data(self, data: Dict[str, Any]):
        self._log.info("Reporting %.0f items ...", len(data.keys()))
        for key, value in data.items():
            self._bus.publish(key, str(value))
