#  CARPI DISPLAY DAEMON
#  (C) 2021, Raphael "rGunti" Guntersweiler
#  Licensed under MIT
from configparser import ConfigParser
from typing import List

SECTION_PING_CHECK = 'PingChecker'
SECTION_INTERFACES = 'InterfaceReporter'


class PingCheckConfig(object):
    """ Configuration for Ping Checker """
    def __init__(self,
                 ips: List[str],
                 interval: float,
                 count: int,
                 enforce_root: bool):
        self.ips: List[str] = ips
        self.interval: float = interval
        self.count: int = count
        self.enforce_root: bool = enforce_root


class InterfaceConfig(object):
    """ Configuration for Interface Reporter """
    def __init__(self,
                 enabled: bool,
                 interval: float,
                 collect_all_interfaces: bool,
                 eth_interface: str,
                 usb_interface: str,
                 wifi_interface: str):
        self.enabled = enabled
        self.interval = interval
        self.collect_all_interfaces = collect_all_interfaces
        self.eth_interface = eth_interface
        self.usb_interface = usb_interface
        self.wifi_interface = wifi_interface


def get_ping_config(config: ConfigParser) -> PingCheckConfig:
    section = config[SECTION_PING_CHECK]
    return PingCheckConfig(section.get('IPs').split(','),
                           section.getfloat('Interval', 300),
                           section.getint('Count', 4),
                           section.getboolean('EnforceRoot', True))


def get_interface_config(config: ConfigParser) -> InterfaceConfig:
    section = config[SECTION_INTERFACES]
    return InterfaceConfig(section.getboolean('CollectInterfaces', fallback=False),
                           section.getfloat('Interval', fallback=5),
                           section.getboolean('CollectAll', fallback=True),
                           section.get('Eth'),
                           section.get('Usb'),
                           section.get('Wifi'))
