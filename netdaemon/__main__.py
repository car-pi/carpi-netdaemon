#  CARPI DISPLAY DAEMON
#  (C) 2021, Raphael "rGunti" Guntersweiler
#  Licensed under MIT
from daemoncommons.daemon import DaemonRunner

from netdaemon.daemon import NetDaemon

if __name__ == '__main__':
    d = DaemonRunner('NETD_CFG', ['netd.ini', '/etc/carpi/netd.ini'])
    d.run(NetDaemon())
